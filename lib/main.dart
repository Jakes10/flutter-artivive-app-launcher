import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:majorprojectarview/homepage.dart';
import 'package:google_fonts/google_fonts.dart';

void main() =>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final textTheme = Theme.of(context).textTheme;

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.grey, // navigation bar color
        statusBarColor: Colors.black12) );
    // status bar color
    return MaterialApp(

      title: 'AR',

      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme:GoogleFonts.latoTextTheme(textTheme).copyWith(
          body1: GoogleFonts.montserrat(textStyle: textTheme.body1),
        ),
      ),
      home: HomePage(),
    );
  }
}