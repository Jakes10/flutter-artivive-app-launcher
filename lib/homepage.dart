import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majorprojectarview/Widget/bezierContainer.dart';
import 'package:url_launcher/url_launcher.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
//   String art ='https://play.google.com/store/apps/details?id=com.artivive';
   String art ='https://www.artivive.com';

//  _launchArtApp() async {
//    const url = 'https://www.artivive.com';
//    if (await canLaunch(url)) {
//      await launch(url);
//    } else {
//      throw 'Could not launch $url';
//    }
//  }

  Future<void> _launchUniversalLink() async {

    if (await canLaunch(art)) {
      final bool succeed= await launch(
        art,
        forceSafariVC: false,
        universalLinksOnly: true,
//          headers: <String, String>{'header_key':'header_value'}
      );
//      if(!succeed){
//        await launch(art, forceWebView: true);
//      }
    } else {
      throw 'Could not launch $art';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[

              Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: MediaQuery.of(context).size.height*0.15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: SizedBox(),
                    ),
                    RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                          text: 'Augmented',
                          style: GoogleFonts.portLligatSans(
                            textStyle: Theme.of(context).textTheme.display1,
                            fontSize: 30,
                            fontWeight: FontWeight.w700,
                            color: Color(0xffe46b10),
                          ),
                          children: [
                            TextSpan(
                              text: ' Reality',
                              style: TextStyle(color: Colors.black, fontSize: 30),
                            ),

                          ]),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Using Marker-Based Augmented reality for Museum Exhibition at the University of Technology, Jamaica.',
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w900,),

                    ),
//                    SizedBox(
//                      height: 20,
//                    ),
//                    _submitButton(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.centerRight,
                          child: Text('Supervisor ',
                              style:
                              TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.centerRight,
                          child: Text('Ms. Anuli',
                              style:
                              TextStyle(fontSize: 14, fontWeight: FontWeight.w900, color: Color(0xffe46b10))),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 60,
                    ),
                    GestureDetector(
                      onTap: (){
                        _launchUniversalLink();
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.symmetric(vertical: 15),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.grey.shade200,
                                  offset: Offset(2, 4),
                                  blurRadius: 5,
                                  spreadRadius: 2)
                            ],
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [Color(0xfffbb448), Color(0xfff7892b)])),
                        child: Text(
                          'Launch App',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                          text: 'Group ',
                          style: GoogleFonts.portLligatSans(
                            textStyle: Theme.of(context).textTheme.display1,
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color:Colors.black ,
                          ),
                          children: [
                            TextSpan(
                              text: 'Members',
                              style: TextStyle(color: Color(0xffe46b10), fontSize: 20),
                            ),
//                            TextSpan(
//                              text: 'ted  Real',
//                              style: TextStyle(color: Color(0xffe46b10), fontSize: 30),
//                            ),

                          ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(

                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            '-Chavoy Davis',
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w900,),

                          ),
                          Text(
                            '-Kehli Cousins',
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w900,),

                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Text(
                            '-Leathon Gregory',
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w900,),

                          ),
                          SizedBox(
                            height: 2,
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Text(
                            '-Sashonie Thomas',
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w900,),

                          ),
                          SizedBox(
                            height: 2,
                          ),

                          Text(
                            '-Jeffery Lynch',
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w900,),

                          ),
                        ],
                      ),
                    ),
//                    _divider(),
//                    _facebookButton(),
//                    Expanded(
//                      flex: 2,
//                      child: SizedBox(),
//                    ),

                  ],
                ),
              ),

              Positioned(
                  top: -MediaQuery.of(context).size.height * .15,
                  right: -MediaQuery.of(context).size.width * .4,
                  child: BezierContainer()),

            ],
          ),
        ),
      ),
    );
  }


}
